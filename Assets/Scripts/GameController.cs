﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MoreMountains.NiceVibrations;
using GameAnalyticsSDK;
using Obi;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static GameController instance;
    public GameObject losePanel;
    public static bool isLose = false;
    public GameObject winPanel;
    public static bool isWin = false;
    public GameObject startMenu;
    public GameObject upgradeMenu;
    public GameObject resultText;
    public GameObject targetProp;
    public Slider progressBar;
    public Text progressText;
    public Text currentLevelText;
    public Text nextLevelText;
    public Text moneyText;
<<<<<<< HEAD
    public int perfect, good, cool;
    public static int numPackLeft = 0;
    public GameObject groundColor1;
    public GameObject groundColor2;
=======
>>>>>>> 5010fca
    public GameObject shopContent;
    public List<GameObject> bladeList = new List<GameObject>();
    public GameObject alertIcon;
    public Mesh[] meshList;
    public GameObject moneyBonus;
<<<<<<< HEAD
    public GameObject colorPack;
    public List<Color> colorListPack = new List<Color>();
    List<Color> checkExistColorPack = new List<Color>();
    public List<Mesh> shapeList = new List<Mesh>();
    public Text taskCanvas;
    public int taskCheck;
    public int taskStandard;
    public float standardVolume;
    public float diffLevel;
    public GameObject pack;

    Rigidbody rigid;
    private Plane planeCut;
    Vector3 start, end, lineVertical, lineHorizontal, left, right;
    Color groundColor;
    GameObject basePivot;
    int currentLevel;
    float money;
    static float size = 0;
    public static float counter = 0;
    bool colorChange = false;
    static bool isCut = false;
    bool isStartGame = false;
    bool isClick = false;

    //Initialize Variables
    public GameObject getTarget;
    Rigidbody targetRb;
    bool isMouseDragging;
    Vector3 offsetValue;
    Vector3 positionOfScreen;
    Vector3 v3Velocity = Vector3.zero;

    Vector3 dist;
    float posX;
    float posY;
    Vector3 startPos;
    Vector3 endPos;
    float dragTime = 0;
    float dragSpeed = 0;
=======
    public List<GameObject> clothList = new List<GameObject>();
    public GameObject Rail;

    Rigidbody rigid;
    GameObject cut0, cut1;
    private Plane plane;
    private MeshCutter cutter;
    GameObject basePivot;
    int currentLevel;
    int money;
    static bool isCut = false;
    public float m_Hue;
    float saturation;
    Color color;
    bool isDrag = false;
    Vector3 pivot;
    Vector3 start, end, lineVertical, lineHorizontal, left, right;
    static bool clickable = true;
    bool isMoveOut = false;
>>>>>>> 5010fca

    // Use this for initialization
    private void Awake()
    {
        instance = this;
        GameAnalytics.Initialize();
<<<<<<< HEAD
        Application.targetFrameRate = 60;
        //PlayerPrefs.DeleteAll();
=======
        Application.targetFrameRate = 60;  
>>>>>>> 5010fca
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("startMenu", 0);
    }

    public void OnButtonStartGame()
    {
        Time.timeScale = 1;
        SoundManager.instance.PlaySound(SoundManager.instance.cut);
        var currentBlade = PlayerPrefs.GetInt("currentBlade");
        GetComponent<MeshFilter>().mesh = meshList[(int)currentBlade];
        this.gameObject.AddComponent<MeshCollider>();
        GetComponent<MeshCollider>().convex = true;
        GetComponent<MeshCollider>().isTrigger = true;
        startMenu.SetActive(false);
    }

    public void OnClickPlay()
    {
        //Swing();
    }

<<<<<<< HEAD
    void Start () {
        instance = this;
        currentLevel = PlayerPrefs.GetInt("currentLevel");

        //FacebookAnalytics.Instance.LogGame_startEvent(1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", currentLevel + 1);

=======
    public void OnChangeCloth()
    {
        StartCoroutine(delayMoveOut(clothList[0]));
        isMoveOut = true;
    }

    IEnumerator delayMoveOut(GameObject cloth)
    {
        //yield return new WaitForSeconds(1);
        cloth.transform.DOMoveY(-20, 2);
        clothList.RemoveAt(0);
        clothList.TrimExcess();
        yield return new WaitForSeconds(2);
        Destroy(cloth.gameObject);
        ChangeProgressValue(1);
        SoundManager.instance.PlaySound(SoundManager.instance.cash);
        isMoveOut = false;
        if (clothList.Count > 0)
        {
            clothList[0].SetActive(true);
            clothList[0].transform.DOMoveY(-10, 3);
            clothList[0].transform.DOMoveX(0, 3);
        }
        TrignometricMovement.instance.Distance.x = Random.Range(0, 0);
        TrignometricMovement.instance.Distance.y = Random.Range(0, 0);
        TrignometricMovement.instance.Distance.z = 5;
        TrignometricMovement.instance.MovementFrequency.x = Random.Range(0, 0);
        TrignometricMovement.instance.MovementFrequency.y = Random.Range(0, 0);
        TrignometricMovement.instance.MovementFrequency.z = Random.Range(1, 2);
    }

    void Start () {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        FacebookAnalytics.Instance.LogGame_startEvent(1);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", currentLevel + 1);
>>>>>>> 5010fca
        var checkStartMenu = PlayerPrefs.GetInt("startMenu");
        if(checkStartMenu == 0)
        {
            Time.timeScale = 0;
            startMenu.SetActive(true);
            PlayerPrefs.SetInt("startMenu", 1);
        }
        else
        {
            Time.timeScale = 1;
            startMenu.SetActive(false);
        }
        for(int i = 1; i < shopContent.transform.childCount; i++)
        {
            bladeList.Add(shopContent.transform.GetChild(i).gameObject);
        }
        money = PlayerPrefs.GetInt("currentMoney");
        var currentBlade = PlayerPrefs.GetInt("currentBlade");
        foreach (var item in bladeList)
        {
            var unlockStatus = PlayerPrefs.GetInt(item.name);
            if(unlockStatus == 1)
            {
                var priceTag = item.transform.GetChild(0).gameObject;
                priceTag.SetActive(false);
            }
            else if(unlockStatus == 0)
            {
                var priceTag = item.transform.GetChild(0).gameObject;
                float price = float.Parse(priceTag.GetComponent<Text>().text.ToString());
                if(money >= price)
                {
                    alertIcon.SetActive(true);
                }
            }
            if (currentBlade == int.Parse(item.name))
            {
                var priceTag = item.transform.GetChild(0).gameObject;
                priceTag.SetActive(false);
                shopContent.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                item.GetComponent<Image>().color = new Color32(255, 247, 35, 255);
            }
        }

        if(!startMenu.activeSelf)
        {
            GetComponent<MeshFilter>().mesh = meshList[(int)currentBlade];
            this.gameObject.AddComponent<MeshCollider>();
            GetComponent<MeshCollider>().convex = true;
            GetComponent<MeshCollider>().isTrigger = true;
        }

        isLose = false;
        isWin = false;
<<<<<<< HEAD
        colorChange = false;
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
        numPackLeft = (currentLevel + 1);
        diffLevel += currentLevel / 100;
        if(diffLevel > 5)
        {
            diffLevel = 5;
        }
        if (numPackLeft > 10)
        {
            numPackLeft = 10;
        }
        var randomColor = Random.Range(0,6);
        switch (randomColor)
        {
            case 0:
                ColorUtility.TryParseHtmlString("#21b8a7", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            case 1:
                ColorUtility.TryParseHtmlString("#F1AF06", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            case 2:
                ColorUtility.TryParseHtmlString("#E46699", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            case 3:
                ColorUtility.TryParseHtmlString("#979ca7", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            case 4:
                ColorUtility.TryParseHtmlString("#C8C8C8", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            case 5:
                ColorUtility.TryParseHtmlString("#6c9ee2", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
            default:
                ColorUtility.TryParseHtmlString("#E46699", out groundColor);
                groundColor1.GetComponent<Renderer>().material.color = groundColor;
                groundColor2.GetComponent<Renderer>().material.color = groundColor;
                break;
        }
=======
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
>>>>>>> 5010fca

        var moneyText1 = startMenu.transform.GetChild(0).GetComponent<Text>();
        var moneyText2 = upgradeMenu.transform.GetChild(0).GetComponent<Text>();
        moneyText1.text = ((int)money).ToString();
        moneyText2.text = ((int)money).ToString();
        moneyText.text = ((int)money).ToString();

<<<<<<< HEAD
        progressBar.maxValue = numPackLeft;
        progressText.text = "0/" + progressBar.maxValue.ToString();
        Physics.gravity = new Vector3(0, -40F, 0);
        rigid = GetComponent<Rigidbody>();
=======
        progressBar.maxValue = clothList.Count;
        Physics.gravity = new Vector3(0, -9.81f, 0);
        rigid = GetComponent<Rigidbody>();
        cutter = new MeshCutter();
>>>>>>> 5010fca
        basePivot = transform.parent.gameObject;
        SpawnPack();
        StartCoroutine(delaySpawnPack());
        //StartCoroutine(loopCut());
    }

<<<<<<< HEAD
    private void FixedUpdate()
    {
        //if (targetRb != null)
        //{
        //    targetRb.velocity = v3Velocity * dragSpeed  * 2 * Time.deltaTime;
        //    //targetRb.AddForce(v3Velocity * dragSpeed);
        //}
        //Mouse Button Press Down
        if (Input.GetMouseButtonDown(0))
        {
            //dragTime = 0;
            RaycastHit hitInfo;
            getTarget = ReturnClickedObject(out hitInfo);
            if (getTarget != null)
            {
                isMouseDragging = true;
                //Converting world position to screen position.
                //targetRb = getTarget.GetComponent<Rigidbody>();
                dist = Camera.main.WorldToScreenPoint(getTarget.transform.position);
                //startPos = getTarget.transform.position;
                posX = Input.mousePosition.x - dist.x;
                posY = Input.mousePosition.y - dist.y;
            }
        }

        //Mouse Button Up
        if (Input.GetMouseButtonUp(0))
        {
            isMouseDragging = false;
            //endPos = getTarget.transform.position;
            //v3Velocity = endPos - startPos;
            //dragSpeed = Vector3.Distance(startPos, endPos) / dragTime;
            //Debug.Log(dragTime + " " + dragSpeed);
        }

        //Is mouse Moving
        if (isMouseDragging)
        {
            //dragTime += Time.deltaTime;
            //Debug.Log(getTarget);
            Vector3 curPos =
                      new Vector3(Input.mousePosition.x - posX,
                      Input.mousePosition.y - posY, dist.z);

            Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);
            if (getTarget != null)
                getTarget.transform.position = new Vector3(getTarget.transform.position.x, getTarget.transform.position.y, worldPos.z);
        }


        //Other way to drag
        //if (Input.GetMouseButton(0))
        //{
        //    RaycastHit hit;
        //    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        //    {
        //        //Debug.Log(hit.collider.name);
        //        if (hit.collider.tag == "Cutable")
        //        {
        //            isMouseDragging = true;
        //            getTarget = hit.collider.gameObject;
        //            getTarget.GetComponent<QuickOutline>().OutlineColor = Color.white;

        //            //dist = Camera.main.WorldToScreenPoint(getTarget.transform.position);
        //            //posX = Input.mousePosition.x - dist.x;
        //            //posY = Input.mousePosition.y - dist.y;
        //        }
        //    }

        //    if (Input.GetMouseButtonUp(0))
        //    {
        //        isMouseDragging = false;
        //    }

        //    if (isMouseDragging)
        //    {
        //        Debug.Log(getTarget.name);
        //        float planeY = 0;

        //        UnityEngine.Plane plane = new UnityEngine.Plane(Vector3.up, Vector3.up * planeY); // ground plane

        //        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //        float distance; // the distance from the ray origin to the ray intersection of the plane
        //        if (plane.Raycast(ray, out distance))
        //        {
        //            getTarget.transform.position = new Vector3(getTarget.transform.position.x, getTarget.transform.position.y, ray.GetPoint(distance).z);
        //        }

        //        //Vector3 curPos =
        //        //      new Vector3(Input.mousePosition.x - posX,
        //        //      Input.mousePosition.y - posY, dist.z);

        //        //Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);
        //        //getTarget.transform.position = new Vector3(getTarget.transform.position.x, getTarget.transform.position.y, worldPos.z);
        //    }
        //}
    }

    //Method to Return Clicked Object
    GameObject ReturnClickedObject(out RaycastHit hit)
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
        {
            if (hit.collider.tag == "Cutable")
            {
                target = hit.collider.gameObject;
                target.GetComponent<QuickOutline>().OutlineColor = Color.black;
            }
        }
        return target;
=======
        m_Hue = Random.Range(0f, 1f);
        RenderSettings.fogColor = Color.HSVToRGB(m_Hue, saturation, .7f);
        StartCoroutine(delayChangeFogColor());
>>>>>>> 5010fca
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isCut && other.tag == "Cutable")
        {
            MMVibrationManager.Vibrate();
            SoundManager.instance.PlaySound(SoundManager.instance.cut);
            isCut = true;
            start = transform.GetChild(0).position;
            end = transform.GetChild(1).position;
            left = transform.GetChild(2).position;
            right = transform.GetChild(3).position;
            lineVertical = start - end;
            lineHorizontal = left - right;
<<<<<<< HEAD
            Plane splitPlane = new Plane(Vector3.Normalize(Vector3.Cross(lineVertical, lineHorizontal)), transform.position);
            other.SendMessage("Split", new Plane[] { splitPlane }, SendMessageOptions.DontRequireReceiver);

            dist = Camera.main.WorldToScreenPoint(getTarget.transform.position);
            //startPos = getTarget.transform.position;
            posX = Input.mousePosition.x - dist.x;
            posY = Input.mousePosition.y - dist.y;
=======
            plane = new Plane(Vector3.Normalize(Vector3.Cross(lineVertical, lineHorizontal)), transform.position);
            //other.SendMessage("Split", new Plane[] { splitPlane }, SendMessageOptions.DontRequireReceiver);
            ContourData contour;
            cutter.Cut(other.gameObject, plane, true, true, out cut0, out cut1, out contour);

            if (cut0 != null)
            {
                cut0.gameObject.tag = "Cutable";
                cut0.AddComponent<Rigidbody>();
                cut0.AddComponent<BoxCollider>();
                cut0.GetComponent<Rigidbody>().mass = 0.1f;
                cut0.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
                cut0.GetComponent<Rigidbody>().freezeRotation = true;
                cut0.GetComponent<Rigidbody>().angularDrag = 0;
                cut0.AddComponent<ObiRigidbody>();
                cut0.AddComponent<ObiCollider>();
            }

            if (cut1 != null)
            {
                cut1.gameObject.tag = "Cutable";
                cut1.AddComponent<Rigidbody>();
                cut1.AddComponent<BoxCollider>();
                cut1.GetComponent<Rigidbody>().mass = 0.1f;
                cut1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
                cut1.GetComponent<Rigidbody>().freezeRotation = true;
                cut1.GetComponent<Rigidbody>().angularDrag = 0;
                cut1.AddComponent<ObiRigidbody>();
                cut1.AddComponent<ObiCollider>();
            }
            //StartCoroutine(delayCut());
>>>>>>> 5010fca
        }

        if(other.tag == "CutConveyor")
        {
            Lose();
        }
    }

    IEnumerator delayCut()
    {
        yield return new WaitForSeconds(0.5f);
        isCut = false;
    }

    private void OnTriggerExit(Collider other)
    {
        //StartCoroutine(delayCut());
        isCut = false;
    }

<<<<<<< HEAD
    public void Lose(string reason)
=======
    public void Lose()
>>>>>>> 5010fca
    {
        if (!isLose && !isWin && taskCheck != taskStandard)
        {
            var progressLose = currentLevel + 1;

            //FacebookAnalytics.Instance.LogGame_endEvent(1);
            //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", progressLose);

            SoundManager.instance.PlaySound(SoundManager.instance.lose);
            losePanel.SetActive(true);
            losePanel.GetComponent<Image>().DOFade(0.95f,1);
            var resultText = losePanel.transform.GetChild(0).GetComponent<Text>();
<<<<<<< HEAD
            resultText.text = reason + "\n" + taskCheck + "/" + taskStandard.ToString(); ;
            var moneyText = losePanel.transform.GetChild(1).GetComponent<Text>();
            moneyText.DOText(((int)money).ToString(), 1);
            PlayerPrefs.SetFloat("currentMoney", money);
=======
            resultText.text = "YOU BREAK THE BLADE";
            var moneyText = losePanel.transform.GetChild(1).GetComponent<Text>();           
            moneyText.DOText(money.ToString(), 1);
            PlayerPrefs.SetInt("currentMoney", money);
>>>>>>> 5010fca
            isLose = true;
            StartCoroutine(delayStopTime(1));
        }
    }

    public void Win()
    {
        if (!isWin && !isLose)
        {
            var progressWin = PlayerPrefs.GetInt("currentLevel");
            progressWin += 1;

            //FacebookAnalytics.Instance.LogGame_endEvent(1);
            //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", progressWin);

            SoundManager.instance.PlaySound(SoundManager.instance.win);
            winPanel.SetActive(true);
            winPanel.GetComponent<Image>().DOFade(0.95f,1);
            var resultText = winPanel.transform.GetChild(0).GetComponent<Text>();
<<<<<<< HEAD
            if(perfect > 5)
            {
                resultText.text = "MASTER";
            }
            else if(good > 5)
            {
                resultText.text = "PROFESSIONAL";
            }
            else
            {
                resultText.text = "NOVICE";
            }
            var moneyText = winPanel.transform.GetChild(1).GetComponent<Text>();
            moneyText.DOText(((int)money).ToString(), 1);
            PlayerPrefs.SetFloat("currentMoney", money);           
=======
            resultText.text = "MASTER CUT";
            var moneyText = winPanel.transform.GetChild(1).GetComponent<Text>();
            moneyText.DOText(money.ToString(), 1);
            PlayerPrefs.SetInt("currentMoney", money);           
>>>>>>> 5010fca
            var currentLevel = PlayerPrefs.GetInt("currentLevel");
            currentLevel++;
            PlayerPrefs.SetInt("currentLevel", currentLevel);
            isWin = true;
            StartCoroutine(delayStopTime(1));
        }
    }

    public void Swing()
    {
<<<<<<< HEAD
        basePivot.transform.DOKill();
        if (basePivot.transform.localRotation.eulerAngles.z <= 360 && basePivot.transform.localRotation.eulerAngles.z >= 330)
=======
        if (basePivot.transform.localRotation.eulerAngles.z <= 360 && basePivot.transform.localRotation.eulerAngles.z >= 330 && clickable)
>>>>>>> 5010fca
        {
            clickable = false;
            basePivot.transform.DOKill();
            Debug.Log("SwingRight");
            basePivot.transform.DORotate(new Vector3(0, 0, 30), 0.5f);
            StartCoroutine(delayClick());
        }
        else if (basePivot.transform.localRotation.eulerAngles.z > 0 && clickable)
        {
            clickable = false;
            basePivot.transform.DOKill();
            Debug.Log("SwingLeft");
            basePivot.transform.DORotate(new Vector3(0, 0, 330), 0.5f);
            StartCoroutine(delayClick());
        }
    }

<<<<<<< HEAD
    IEnumerator loopCut()
    {
        yield return new WaitForSeconds(1f);
        Swing();
        StartCoroutine(loopCut());
    }

    public void ChangeProgressValue(float value)
=======
    IEnumerator delayClick()
    {
        yield return new WaitForSeconds(0.1f);
        clickable = true;
    }

    public void ChangeProgressValue(int value)
>>>>>>> 5010fca
    {
        progressBar.value += value;
        progressText.text = progressBar.value.ToString() + "/" + progressBar.maxValue.ToString();
        if (progressBar.value >= progressBar.maxValue)
        {
            Win();
        }
    }

<<<<<<< HEAD
    public void PlusMoney(float moneyPlus)
    {
        StartCoroutine(delayGainMoneyEffect(moneyPlus));
=======
    public void ChangeMoney(int bonus)
    {             
        moneyBonus.GetComponent<Text>().text = "+" + (int)bonus;
        StartCoroutine(delayGainMoneyEffect(bonus));
>>>>>>> 5010fca
    }

    public void OnButtonNextLevel()
    {
        SoundManager.instance.PlaySound(SoundManager.instance.cut);
        SceneManager.LoadScene(0);
    }

    public void OnButtonChangeBlade()
    {
        var currentButton = EventSystem.current.currentSelectedGameObject;
        string bladeString = currentButton.gameObject.name.ToString();
        int bladeNumber = int.Parse(bladeString);
        var unlockStatus = PlayerPrefs.GetInt(bladeString);
        if (unlockStatus == 1 || bladeNumber == 0)
        {
            PlayerPrefs.SetInt("currentBlade", bladeNumber);
            currentButton.GetComponent<Image>().color = new Color32(255, 247, 35, 255);
            SoundManager.instance.PlaySound(SoundManager.instance.cut);
            if(bladeNumber != 0)
            {
                shopContent.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            }
                foreach(var item in bladeList)
                {
                    if(item.name != currentButton.name)
                    {
                        item.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    }
                }
        }
        else if(unlockStatus == 0 && bladeNumber != 0)
        {
            var priceTag = currentButton.transform.GetChild(0).gameObject;
            var price = int.Parse(priceTag.GetComponent<Text>().text.ToString());
            money = PlayerPrefs.GetInt("currentMoney");
            if(money >= price)
            {
                money -= price;
                PlayerPrefs.SetInt("currentMoney", money);
                PlayerPrefs.SetInt(bladeString, 1);
                var moneyText2 = upgradeMenu.transform.GetChild(0).GetComponent<Text>();
                moneyText2.text = ((int)money).ToString();
                var moneyText1 = startMenu.transform.GetChild(0).GetComponent<Text>();
                moneyText1.text = ((int)money).ToString();
                priceTag.SetActive(false);
                PlayerPrefs.SetInt("currentBlade", bladeNumber);
                currentButton.GetComponent<Image>().color = new Color32(255, 247, 35, 255);
                SoundManager.instance.PlaySound(SoundManager.instance.score);
                shopContent.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                foreach(var item in bladeList)
                {
                    if(item.name != currentButton.name)
                    {
                        item.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    }
                }
            }
            else
            {
                SoundManager.instance.PlaySound(SoundManager.instance.error);
            }
        }
    }

<<<<<<< HEAD
    public void SpawnPack()
    {
        float maxPendulumSpeed = 5 + currentLevel / 20;
        if(maxPendulumSpeed > 7)
        {
            maxPendulumSpeed = 7;
        }
        Pendulum.instance.speed = Random.Range(3f, maxPendulumSpeed);
        float min = 50 - currentLevel / 2;
        if(min < 20)
        {
            min = 20;
        }
        var randomSize = Random.Range(5, 12f);
        pack = Instantiate(colorPack, new Vector3(0, 1.25f, -20), Quaternion.Euler(new Vector3(0, 0, 0)));
        pack.transform.localScale = new Vector3(1, 1, randomSize);
        taskStandard = (int)(randomSize/1 + diffLevel);
        taskCheck = 0;
        taskCanvas.text = taskCheck + "/" + taskStandard.ToString();
        //var randomShape = Random.Range(0, shapeList.Count);
        //pack.GetComponent<MeshFilter>().sharedMesh = shapeList[randomShape];
        //pack.GetComponent<MeshCollider>().sharedMesh = shapeList[randomShape];
        var color = colorListPack[Random.Range(0, colorListPack.Count - 1)];
        pack.GetComponent<Renderer>().material.color = color;
        checkExistColorPack.Add(color);
        colorListPack.Remove(color);
        if (colorListPack.Count == 0)
        {
            foreach (var item in checkExistColorPack)
            {
                colorListPack.Add(item);
            }
            checkExistColorPack.Clear();
        }
        else
            colorListPack.TrimExcess();

        isStartGame = true;
        Mesh mesh = pack.GetComponent<MeshFilter>().sharedMesh;
        standardVolume = VolumeOfMesh(mesh);
        standardVolume /= 1;
        pack.transform.DOMoveZ(-10.5f /*- randomSize/2*/, 0.5f);
    }

    IEnumerator delaySpawnPack()
    {
        while(taskCheck < taskStandard)
        {
            yield return null;
        }
        ChangeProgressValue(1);
        //GameObject[] allPacks = GameObject.FindGameObjectsWithTag("Cutable");
        //foreach(var item in allPacks)
        //{
        //    item.GetComponent<Renderer>().material.DOFade(0, 1);
        //    Destroy(item, 1);
        //}
        TaskCheck.isClear = true;
        yield return new WaitForSeconds(1f);
        SpawnPack();
        StartCoroutine(delaySpawnPack());
    }

=======
>>>>>>> 5010fca
    IEnumerator delayLoadScene()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);
    }

    IEnumerator delayStopTime(float time)
    {
        var buttonLose = losePanel.transform.GetChild(3).gameObject;
        var buttonWin = winPanel.transform.GetChild(3).gameObject;
        yield return new WaitForSeconds(time);
        buttonLose.SetActive(true);
        buttonWin.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 0;
    }

<<<<<<< HEAD
    IEnumerator delayGainMoneyEffect(float moneyPlus)
=======
    IEnumerator delayGainMoneyEffect(int bonus)
>>>>>>> 5010fca
    {
        moneyBonus.GetComponent<Text>().text = "+" + moneyPlus.ToString();
        moneyBonus.transform.DOKill();
        moneyBonus.transform.localScale = new Vector3(1, 1, 1);
        moneyBonus.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20, 450, 0);
        yield return new WaitForSeconds(0.5f);
        moneyBonus.transform.DOMove(moneyText.transform.position, 0.5f);
        moneyBonus.transform.DOScale(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
<<<<<<< HEAD
        money += moneyPlus;
        moneyText.text = ((int)money).ToString();
=======
        money += bonus;
        moneyText.text = money.ToString();
>>>>>>> 5010fca
    }

    public void OnChangeLevelTest()
    {
        var currentButton = EventSystem.current.currentSelectedGameObject;
        var input = currentButton.GetComponent<InputField>().text;
        var level = int.Parse(input);
        PlayerPrefs.SetInt("currentLevel", level);
        SceneManager.LoadScene(0);
    }

<<<<<<< HEAD
    public float SignedVolumeOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float v321 = p3.x * p2.y * p1.z;
        float v231 = p2.x * p3.y * p1.z;
        float v312 = p3.x * p1.y * p2.z;
        float v132 = p1.x * p3.y * p2.z;
        float v213 = p2.x * p1.y * p3.z;
        float v123 = p1.x * p2.y * p3.z;
        return (1.0f / 6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
    }
    public float VolumeOfMesh(Mesh mesh)
    {
        float volume = 0;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Vector3 p1 = vertices[triangles[i + 0]];
            Vector3 p2 = vertices[triangles[i + 1]];
            Vector3 p3 = vertices[triangles[i + 2]];
            volume += SignedVolumeOfTriangle(p1, p2, p3);
        }
        return Mathf.Abs(volume);
=======
    public void SetColor()
    {
        //increment the hue, looping back to 0 if necessary
        m_Hue += (1f / 100f);
        if (m_Hue > 1f)
            m_Hue = 0;
        //Default saturation
        saturation = .7f;
        // Set color for each child's renderer
        color = Color.HSVToRGB(m_Hue, saturation, .7f);
    }

    IEnumerator delayChangeFogColor()
    {
        yield return new WaitForSeconds(1);
        SetColor();
        DOTween.To(() => RenderSettings.fogColor, x => RenderSettings.fogColor = x, color, 1f);
        StartCoroutine(delayChangeFogColor());
>>>>>>> 5010fca
    }
}
