﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LaneGenerator : MonoBehaviour {
    public float maxVariance = 0f;
    public int count = 16;
    public GameObject Object;

    public List<GameObject> m_ColumnList;
    private int m_OldestColumnIndex;
    private int m_NewestColumnIndex;
    private float m_LastAppliedBallZ;
    private int targetCounter;
    private float m_LaneSize;
    int targetFrequency = 20;

    // Use this for initialization
    void Start () {
        m_LaneSize = Object.transform.localScale.z;
        m_ColumnList = new List<GameObject>();
        for (int i = 0; i < count; i++)
        {
            if (i == 0)
            {
                m_ColumnList.Add(Object);
            }
            else
            {
                GameObject newColumn = Instantiate(Object);
                newColumn.transform.parent = Object.transform.parent;
                m_ColumnList.Add(newColumn);
                PlaceColumn(m_ColumnList.Count - 1, m_ColumnList[m_ColumnList.Count - 2].transform.position, maxVariance);
            }
        }
        m_OldestColumnIndex = 0;
        m_NewestColumnIndex = m_ColumnList.Count - 1;
    }

    private void PlaceColumn(int columnIndex, Vector3 prevColumnPos, float maximumVariance )
    {
        float offsetY = 0;
        offsetY = Random.Range(prevColumnPos.y - maximumVariance, prevColumnPos.y + maximumVariance);
        if(offsetY > 2 || offsetY < -2)
        {
            offsetY = 0;
        }
        Vector3 movePos = new Vector3(prevColumnPos.x, offsetY, prevColumnPos.z + m_LaneSize);
        m_ColumnList[columnIndex].transform.position = movePos;
        targetCounter++;
        if (m_ColumnList[columnIndex].transform.Find(columnIndex.ToString()))
        {
            Destroy(m_ColumnList[columnIndex].transform.Find(columnIndex.ToString()).gameObject);
        }
    }
}
