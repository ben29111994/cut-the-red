﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Obi;

<<<<<<< HEAD
public class Scoring : MonoBehaviour
{
=======
public class Scoring : MonoBehaviour {
>>>>>>> 5010fca

    public GameObject rateText;
    public List<GameObject> existPackage = new List<GameObject>();
    GameObject player;
<<<<<<< HEAD
    float cutVolume;
    float bonusMoney;

    // Use this for initialization
    void Start()
    {
        var currentLevel = PlayerPrefs.GetInt("currentLevel");
        player = GameObject.FindGameObjectWithTag("Player");
        rateText = GameObject.FindGameObjectWithTag("RateText");
=======
    int standardVolume;
    public float hue = 0.5f;
    float hueValuePerPack = 0;
    int money;

	// Use this for initialization
	void Start () {
        var currentLevel = PlayerPrefs.GetInt("currentLevel");
        player = GameObject.FindGameObjectWithTag("Player");

        standardVolume = Random.Range(3, 3 + currentLevel);
        money = standardVolume;
        hueValuePerPack = 0.5f / standardVolume;
        rateText = GameObject.FindGameObjectWithTag("RateText");
        rateText.GetComponent<Text>().text = standardVolume.ToString();
        rateText.GetComponent<Text>().DOKill();
        rateText.GetComponent<Text>().DOFade(1, 0.5f);
>>>>>>> 5010fca
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cutable")
        {
<<<<<<< HEAD
            player.GetComponent<GameController>().taskCheck++;
            player.GetComponent<GameController>().taskCanvas.text = player.GetComponent<GameController>().taskCheck + "/" + player.GetComponent<GameController>().taskStandard.ToString();
            if (player.GetComponent<GameController>().taskCheck > player.GetComponent<GameController>().taskStandard)
            {
                player.GetComponent<GameController>().Lose("OVER CUT");
            }

            TaskCheck.counter--;
            //Debug.Log(TaskCheck.counter);
            //Debug.Log(other.name);
            if (TaskCheck.counter <= 0)
            {
                GameController.instance.Lose("UNDER CUT");
            }

            if (!existPackage.Contains(other.gameObject))
            {
                existPackage.Add(other.gameObject);
                Mesh mesh = other.GetComponent<MeshFilter>().sharedMesh;
                cutVolume = VolumeOfMesh(mesh);
                var rate = Mathf.Abs(cutVolume - player.GetComponent<GameController>().standardVolume);
                //Debug.Log(cutVolume + " - " + player.GetComponent<GameController>().standardVolume + " = " + rate);
                if(rate < 0.0075f)
                {
                    rateText.GetComponent<Text>().text = "PERFECT";
                    rateText.GetComponent<Text>().color = Color.magenta;
                    bonusMoney = 3;
                    player.GetComponent<GameController>().perfect++;
                }
                else if (rate < 0.01)
                {
                    rateText.GetComponent<Text>().text = "GOOD";
                    rateText.GetComponent<Text>().color = Color.green;
                    bonusMoney = 2;
                    player.GetComponent<GameController>().good++;
                }
                else
                {
                    rateText.GetComponent<Text>().text = "COOL";
                    rateText.GetComponent<Text>().color = Color.cyan;
                    bonusMoney = 1;
                    player.GetComponent<GameController>().cool++;
                }
                SoundManager.instance.PlaySound(SoundManager.instance.score);
                rateText.GetComponent<Text>().DOFade(1, 0);
                rateText.transform.DOScale(1, 0.5f).SetLoops(1, LoopType.Yoyo);
                rateText.GetComponent<Text>().DOFade(0, 3);
            }
            //other.gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        player.GetComponent<GameController>().PlusMoney(bonusMoney);
        SoundManager.instance.PlaySound(SoundManager.instance.cash);
        other.tag = "Untagged";
        Destroy(other.gameObject, 5);
    }

    public float SignedVolumeOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float v321 = p3.x * p2.y * p1.z;
        float v231 = p2.x * p3.y * p1.z;
        float v312 = p3.x * p1.y * p2.z;
        float v132 = p1.x * p3.y * p2.z;
        float v213 = p2.x * p1.y * p3.z;
        float v123 = p1.x * p2.y * p3.z;
        return (1.0f / 6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
    }
    public float VolumeOfMesh(Mesh mesh)
    {
        float volume = 0;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Vector3 p1 = vertices[triangles[i + 0]];
            Vector3 p2 = vertices[triangles[i + 1]];
            Vector3 p3 = vertices[triangles[i + 2]];
            volume += SignedVolumeOfTriangle(p1, p2, p3);
=======
            if (standardVolume > 0)
            {
                if (!existPackage.Contains(other.gameObject))
                {
                    existPackage.Add(other.gameObject);
                    other.transform.parent = transform;
                    other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                    standardVolume--;
                    if (standardVolume <= 0)
                    {
                        standardVolume = 0;
                        GameController.instance.OnChangeCloth();
                        GameController.instance.ChangeMoney(money);
                        rateText.GetComponent<Text>().DOFade(0, 1);
                    }
                    hue -= hueValuePerPack;
                    Debug.Log(hue);
                    if (hue < 0)
                    {
                        hue = 0;
                    }
                    if (hue > 0.5f)
                    {
                        hue = 0.5f;
                    }
                    GetComponent<Renderer>().materials[0].DOColor(Color.HSVToRGB(hue, .7f, .7f), 0.5f);
                    SoundManager.instance.PlaySound(SoundManager.instance.score);
                    rateText.GetComponent<Text>().text = standardVolume.ToString();
                }
            }
>>>>>>> 5010fca
        }
    }
}
