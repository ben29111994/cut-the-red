﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource audioSource;
    public AudioClip cut, score, error, win, lose, cash;	

	void Awake(){
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        try
        {
            audioSource = GetComponent<AudioSource>();
        }
        catch
        {

        }
	}

	public void PlaySound(AudioClip clip){
		audioSource.PlayOneShot(clip);
	}
}
