﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDisable : MonoBehaviour
{
    //Start is called before the first frame update
    void OnEnable()
    {
        if (tag == "Cutable")
        {
            TaskCheck.counter++;
            //Debug.Log(TaskCheck.counter);
            //Debug.Log(gameObject.name);
        }
    }

    void OnCollisionStay(Collision other)
    {
        if(other.gameObject.tag == "CutConveyor")
        {
            GameController.instance.getTarget = this.gameObject;
        }
    }

    void OnDisable()
    {
        if (tag == "Cutable")
        {
            //TaskCheck.instance.ChangeCounter();
            TaskCheck.counter--;
            //Debug.Log(TaskCheck.counter);
            //Debug.Log(gameObject.name);
        }
        //if (TaskCheck.counter <= 0)
        //{
        //    GameController.instance.Lose("UNDER CUT");
        //}
    }
}
