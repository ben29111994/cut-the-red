﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class LinearConveyor : MonoBehaviour {
    public static LinearConveyor instance;
    public GameObject player;
    public GameController gameController;
    public float speed = 1f;
<<<<<<< HEAD
    float distance = 1f;
=======
    public float distance = 1f;
>>>>>>> 5010fca
    public bool instancedMaterial;
    Rigidbody rb;
    Collider col;
    MeshRenderer mr;
    int currentLevel;
    bool isChangeSpeed = false;
<<<<<<< HEAD

    // Use this for initialization
    void Start() {
        currentLevel = PlayerPrefs.GetInt("currentLevel");
=======
    bool isStop = false;

    // Use this for initialization
    void Start() {
        RefreshReferences();
>>>>>>> 5010fca
        player = GameObject.FindGameObjectWithTag("Player");
        gameController = player.GetComponent<GameController>();
        currentLevel = PlayerPrefs.GetInt("currentLevel");
<<<<<<< HEAD
        speed = 5f;
        if (this.tag == "CutConveyor")
        {
            speed = /*5f + currentLevel/20;*/0;
            //if(speed > 8)
            //{
            //    speed = 8;
            //}
        }
=======
        speed = 3f;
>>>>>>> 5010fca
        ChangeSpeed(speed);
    }

    public void RefreshReferences() {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        col = gameObject.GetComponent<Collider>();
        if (col == null) {
            col = gameObject.AddComponent<MeshCollider>();
        }

        mr = gameObject.GetComponent<MeshRenderer>();
        if (mr == null)
            mr = gameObject.GetComponentInChildren<MeshRenderer>();
        if (mr == null)
            Debug.LogError("Linear Conveyor needs to be attached to the belt Object");
    }

    // Update is called once per frame
    void FixedUpdate() {
        Vector3 mov = transform.forward * Time.deltaTime * speed / distance;
        rb.position = (rb.position - mov);
        rb.MovePosition(rb.position + mov);
    }

    public void ChangeSpeed(float _speed) {
        speed = _speed;
        if (instancedMaterial) {
            Material tempMat = new Material(mr.sharedMaterial);
            tempMat.SetFloat("_Speed", speed);
            mr.material = tempMat;
        } else {
            mr.sharedMaterial.SetFloat("_Speed", speed);
        }
    }
}
